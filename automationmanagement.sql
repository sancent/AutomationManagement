/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50542
 Source Host           : localhost
 Source Database       : automationmanagement

 Target Server Type    : MySQL
 Target Server Version : 50542
 File Encoding         : utf-8

 Date: 08/05/2018 23:57:59 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `hosts`
-- ----------------------------
DROP TABLE IF EXISTS `hosts`;
CREATE TABLE `hosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `ip` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `load_key_sorts`
-- ----------------------------
DROP TABLE IF EXISTS `load_key_sorts`;
CREATE TABLE `load_key_sorts` (
  `id` int(11) DEFAULT NULL,
  `local_name` varchar(255) DEFAULT NULL COMMENT '识别名称',
  `app_key_sort` text COMMENT '排序关键词'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_机构分区管理`
-- ----------------------------
DROP TABLE IF EXISTS `pre_机构分区管理`;
CREATE TABLE `pre_机构分区管理` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_生产数据库授权账号列表`
-- ----------------------------
DROP TABLE IF EXISTS `pre_生产数据库授权账号列表`;
CREATE TABLE `pre_生产数据库授权账号列表` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_aaaaa`
-- ----------------------------
DROP TABLE IF EXISTS `pre_aaaaa`;
CREATE TABLE `pre_aaaaa` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `test` varchar(100) DEFAULT '' COMMENT 'test',
  `testb` varchar(100) DEFAULT '' COMMENT 'testb',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_add_module`
-- ----------------------------
DROP TABLE IF EXISTS `pre_add_module`;
CREATE TABLE `pre_add_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `model_name` varchar(64) DEFAULT NULL COMMENT '自定义模块的名称',
  `main_title` varchar(64) NOT NULL COMMENT '首页第一个标题',
  `next_title` varchar(64) DEFAULT NULL COMMENT '首页显示第二个位置',
  `main_table_title` varchar(64) NOT NULL COMMENT '列表的大标题',
  `model_tr_string` varchar(255) DEFAULT NULL COMMENT '列表的详细内容字段',
  `drop_down_id` varchar(128) DEFAULT '0' COMMENT '下拉框的字段对应的id',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `pre_add_module`
-- ----------------------------
BEGIN;
INSERT INTO `pre_add_module` VALUES ('1', 'equipment', '账号管理', '设备管理', '内网相关设备信息管理', 'id,equipment_name,equipment_type,admin_account,admin_passwd,name', '1', '2014-11-27 11:24:19', '2014-11-27 11:24:19', '启用', ''), ('2', 'dropdown', '模块管理', '下拉框管理', '下拉框信息管理', 'id,id_key,main_string,main_drop_content', '2', '2014-12-04 14:12:42', '2014-12-04 14:12:42', '启用', ''), ('3', 'addstr', '模块管理', '字段管理', '字段信息管理', 'id,str_table,str_tables_desc,str_name,str_des,str_type,str_lenth,is_success', '3', '2014-12-05 11:21:32', '2014-12-05 11:21:32', '启用', ''), ('14', 'tbmanage', '模块管理', '数据表管理', '数据库表管理', 'id,tablename,tabledesc,note', '14', '2014-12-09 11:56:30', '2014-12-09 11:56:30', '启用', ''), ('16', 'menus', '模块管理', '菜单管理', '左侧菜单管理', 'id,title,name,link,sort,status,pid,level', '16', '2014-12-11 11:36:17', '2014-12-11 11:36:17', '启用', ''), ('17', 'b2cnet', '主机管理', 'B2C', 'b2c-net环境服务器信息管理', 'id,depart_name,server_name,gongwang_ip,network_ip,config_dir,pro_dir,log_dir,created_time,status,note', '17', '2014-12-19 10:28:59', '2014-12-19 10:28:59', '启用', ''), ('20', 'unusedcomputer', '主机管理', '备用虚拟机', '备用虚拟机', 'id,depart_name,server_name,network_ip,cpu,mem,disk,status,note', '20', '2014-12-23 15:24:11', '2014-12-23 15:24:11', '启用', ''), ('22', 'checksvnbackup', '巡检管理', 'svn备份检查', 'svn备份检查', 'id,check_man,check_server,check_svnlocalbkinfo,check_svnbackup,check_info', '22', '2014-12-23 16:29:50', '2014-12-23 16:29:50', '启用', ''), ('26', 'propertiesofsvn', 'svn数据备份基本属性信息', 'svn数据备份基本属性信息', 'svn数据备份基本属性信息', 'id,svn_server,svn_path,svn_size,svn_localbackuppath,svn_localkeeptime,svn_distancebackupserver,svn_distancebackuppath,svn_distancekeeptime,created_time,updated_time,status,note', '26', '2014-12-31 17:35:08', '2014-12-31 17:35:08', '启用', ''), ('27', 'svnresourceslist', 'svn资源列表（显示svn下工程目录及相关信息）', 'svn资源列表', 'svn资源列表', 'id,priresoulibname,primarysource,size,pridescription,secresoulibname,secdescription', '27', '2015-01-05 15:53:23', '2015-01-05 15:53:23', '启用', ''), ('28', 'newframework', '主机管理', '新框架', '新框架', 'id,depart_name,server_name,network_ip,config_dir,pro_dir,log_dir,created_time,status,note', '28', '2015-01-26 15:42:45', '2015-01-26 15:42:45', '启用', ''), ('29', 'xenserverlist', '主机管理', '机房物理服务器列表', '机房物理服务器列表', 'id,type/version,ipaddr,account/passwd,t-mem/re-mem,t-disk/re-disk,vm,createvm,status,note', '29', '2015-02-02 20:28:57', '2015-02-02 20:28:57', '启用', ''), ('30', 'pc', '仓库PC机管理', '主机管理', '仓库PC机管理', 'id,encoding,displayencoding,configuration,status,usestaus,updated_time,note', '30', '2015-03-09 11:20:22', '2015-03-09 11:20:22', '启用', ''), ('31', 'fault52zzb', '准生产故障处理记录', '故障管理', '准生产故障处理记录', 'id,dealwith_man,application,fault_desc,fault_type,recovery,influence_time,fault_level,created_time,note', '31', '2015-03-31 16:21:00', '2015-03-31 16:21:00', '启用', ''), ('34', 'ediipguanli', 'edi端口ip管理', 'edi端口ip管理', 'edi端口ip管理', 'id,52zzborcom,baoxiangongsi,ip,edi,akka,created_time,updated_time,status,note', '34', '2015-04-09 18:49:41', '2015-04-09 18:49:41', '启用', ''), ('37', 'jigouguanli', '机构管理', '机构管理', '机构管理', 'id,fenqu,name,code,created_time,updated_time,status,note', '37', '2015-04-13 15:05:12', '2015-04-13 15:05:12', '启用', ''), ('38', 'baowangshebei', '保网SNMP、设备管理', '保网SNMP、设备管理', '保网SNMP、设备管理', 'id,snmp,account,password,person,mouse,keyboard,usemouse,usekeyboard,created_time', '38', '2015-04-13 15:08:38', '2015-04-13 15:08:38', '启用', ''), ('39', 'wuxianjiankong', '无线信号监控', '无线信号监控', '无线信号监控', 'id,ap,admin,password,ssid,passwd,spectrum,address,floor', '39', '2015-04-15 14:29:44', '2015-04-15 14:29:44', '启用', ''), ('47', 'comgrant', '生产数据库授权列表', '生产数据库授权列表', '生产数据库授权列表', 'id,IP,mysqlleixing,zhanghao,fuquanadd,mysql,fuquandaxiao,status,created_time,updated_time,note', '47', '2015-05-25 12:42:41', '2015-05-25 12:42:41', '启用', ''), ('48', 'commysql', '生产数据库使用情况', '生产数据库使用情况', '生产数据库使用情况', 'id,gongwangip,neiwangip,user,shilie,shujku,yingyongmingcheng', '41', '2015-05-25 13:00:53', '2015-05-25 13:00:53', '启用', ''), ('50', 'bwzzbmonitor', '监控管理', '准生产环境', '准生产环境应用监控管理', 'id,app_key,app_cpu,app_mem,process_num,app_port,app_status,app_disk,app_logs,updated_time', '50', '2015-06-08 09:56:16', '2015-06-08 09:56:16', '启用', ''), ('59', 'jianchakb', '巡检管理', 'kb备份', 'kb备份', 'id,check_man,check_server,check_kblocalbkinfo,check_kbbackup,check_info', '59', '2015-06-23 14:53:34', '2015-06-23 14:53:34', '启用', ''), ('60', 'ipmac', 'ip-mac-person对应表', 'ip-mac-person对应表', 'ip-mac-person对应表', 'id,ip,mac,person,status,note', '60', '2015-07-06 09:33:12', '2015-07-06 09:33:12', '启用', ''), ('61', 'backtest', '主机管理', '测试和备份主机', '测试和备份主机', 'id,user,ip,created_time,status,note', '61', '2015-07-17 10:39:10', '2015-07-17 10:39:10', '启用', ''), ('62', 'qunabaoorg', '主机管理', '去哪保-org环境', '去哪保-org环境', 'id,depart_name,server_name,network_ip,mem,disk,pro_type,gongcheng_dir,config_dir,pro_init,pro_port,system_type,created_time,updated_time,status,note,mask', '62', '2015-08-10 09:09:49', '2015-08-10 09:09:49', '启用', ''), ('63', 'qunabaocom', '主机管理', '去哪保-com环境', '去哪保-com环境', 'id,depart_name,server_name,gongwang_ip,network_ip,mem,disk,pro_type,config_dir,pro_init,pro_port,system_type,created_time,updated_time,status,note,mask', '63', '2015-08-10 15:37:42', '2015-08-10 15:37:42', '启用', ''), ('64', 'rm_shuju', '主机管理', 'redis&mongodb信息', 'redis&mongodb信息', 'id,gongwang,neiwang,yingyong,denglu,user_password,created_time,updated_time,status,note', '64', '2015-11-24 15:25:55', '2015-11-24 15:25:55', '启用', ''), ('70', 'ldapbeifen', 'ldap备份检查', 'ldap备份检查', 'ldap备份检查', 'id,startdate,enddate,caldata,reson,note', '70', '2016-03-25 16:59:36', '2016-03-25 16:59:36', '启用', ''), ('71', 'db_checkmysql', '巡检管理', 'mysql备份情况', 'mysql备份情况', 'id,check_people,check_network_ip,check_content,check_date,check_size,check_stuation,created_time,updated_time,status,note', '71', '2016-03-25 17:35:09', '2016-03-25 17:35:09', '启用', ''), ('72', 'financialcloud', '金融云', '金融云', '金融云', 'id,depart_name,server_name,gongwang_ip,network_ip,config_dir,pro_dir,log_dir,created_time,status,note', '72', '2016-05-03 17:23:08', '2016-05-03 17:23:08', '启用', ''), ('74', 'xinkuangjia_com', '新框架生产', '新框架生产', '新框架生产', 'id,depart_name,server_name,gongwang_ip,network_ip,config_dir,pro_dir,log_dir,created_time,status,note', '74', '2016-05-06 14:24:25', '2016-05-06 14:24:25', '启用', ''), ('76', 'yanfarecord', '研发人员', '研发人员', '研发人员', 'id,created_time,updated_time,dep,name,status,sys,func,tel,note', '76', '2016-05-10 10:52:30', '2016-05-10 10:52:30', '启用', ''), ('78', 'roboot', '新框架生产精灵服务器清单', '新框架生产精灵服务器清单', '新框架生产精灵服务器清单', 'id,addr,company,type,pub_ip,pri_ip,key_1,key_2,line,vpn,ava,res,state_now,user_x,passwd_x,created_time,updated_time,status,note', '78', '2016-05-10 18:08:26', '2016-05-10 18:08:26', '启用', ''), ('79', 'svn_uplist', '升级管理列表', '升级管理列表', '升级管理列表', 'id,sid,bug,num,developer,person,sys,fun,status,sta_1,sta_2,yn,time', '79', '2016-05-13 14:59:16', '2016-05-13 14:59:16', '启用', ''), ('82', 'deploy_manage', '自动化发布管理', '自动化发布管理', '自动化发布管理', 'id,deploy_sys,deploy_people,deploy_ip,deploy_content,deloy_size,deploy_take,processNumber', '82', '2016-05-16 15:57:15', '2016-05-16 15:57:15', '启用', ''), ('83', 'deploy_status', '自动化发布状态', '自动化发布状态', '自动化发布状态', 'id,deploy_sys,deploy_people,deploy_ip,deploy_content,deploy_take,status,note', '83', '2016-05-16 16:25:03', '2016-05-16 16:25:03', '启用', ''), ('84', 'olddataback', '旧数据保留情况', '旧数据保留情况', '旧数据保留情况', 'id,fenqu,mingcheng,backmachine,backstyle,backpath,created_time,updated_time,status,note', '84', '2016-10-21 09:45:03', '2016-10-21 09:45:03', '启用', '');
COMMIT;

-- ----------------------------
--  Table structure for `pre_addstr`
-- ----------------------------
DROP TABLE IF EXISTS `pre_addstr`;
CREATE TABLE `pre_addstr` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `str_table` varchar(64) DEFAULT NULL COMMENT '字段所属表',
  `str_tables_desc` varchar(128) DEFAULT NULL COMMENT '字段所属表描述',
  `str_name` varchar(64) DEFAULT NULL COMMENT '字段名称',
  `str_des` varchar(64) NOT NULL COMMENT '字段标题',
  `str_type` varchar(64) DEFAULT NULL COMMENT '字段类型',
  `str_lenth` varchar(64) NOT NULL COMMENT '字段长度',
  `str_define` varchar(64) DEFAULT NULL COMMENT '字段默认值',
  `is_success` varchar(64) DEFAULT '否' COMMENT '是否已添加',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `pre_addstr`
-- ----------------------------
BEGIN;
INSERT INTO `pre_addstr` VALUES ('5', 'testtab', '测试描述', 'teststring', '测试字段', '字符型', '64', '', '是', '2014-12-05 17:40:13', '2014-12-05 17:40:13', '开启', ''), ('8', 'testtab', '测试描述', 'add_str', '测试添加', '字符型', '100', '测试', '是', '2014-12-08 11:37:51', '2014-12-08 11:37:51', '启动', ''), ('14', 'testa', '测试', 'testa', '测试字段', '字符型', '100', '', '是', '2014-12-22 09:28:37', '2014-12-22 09:28:37', '启动', ''), ('28', 'checksvnbackup', '', 'check_man', '检测人', '字符型', '32', '', '是', '2014-12-23 16:32:35', '2014-12-23 16:32:35', '启动', ''), ('29', 'checksvnbackup', '', 'check_server', '检测的服务器', '字符型', '64', '', '是', '2014-12-23 16:37:16', '2014-12-23 16:37:16', '启动', ''), ('30', 'checksvnbackup', '', 'check_svnbackup', '检测svn备份', '字符型', '100', '', '是', '2014-12-23 16:38:27', '2014-12-23 16:38:27', '启动', ''), ('31', 'checksvnbackup', '', 'check_info', '检测信息', '字符型', '100', '', '是', '2014-12-23 16:39:36', '2014-12-23 16:39:36', '启动', ''), ('32', 'propertiesofsvn', '', 'svn_server', 'svn服务器', '字符型', '100', '', '是', '2014-12-31 15:46:20', '2014-12-31 15:46:20', '启动', ''), ('33', 'propertiesofsvn', '', 'svn_path', 'svn数据路径', '字符型', '100', '', '是', '2014-12-31 15:48:02', '2014-12-31 15:48:02', '启动', ''), ('34', 'propertiesofsvn', '', 'svn_size', 'svn数据大小', '字符型', '100', '', '是', '2014-12-31 15:48:51', '2014-12-31 15:48:51', '启动', ''), ('35', 'propertiesofsvn', '', 'svn_localbackuppath', 'svn本地数据备份路径', '字符型', '100', '', '是', '2014-12-31 15:49:19', '2014-12-31 15:49:19', '启动', ''), ('36', 'propertiesofsvn', '', 'svn_localkeeptime', 'svn本地数据备份保留时间', '字符型', '100', '', '是', '2014-12-31 15:50:17', '2014-12-31 15:50:17', '启动', ''), ('37', 'propertiesofsvn', '', 'svn_distancebackupserver', 'svn异地备份服务器', '字符型', '100', '', '是', '2014-12-31 15:51:24', '2014-12-31 15:51:24', '启动', ''), ('38', 'propertiesofsvn', '', 'svn_distancebackuppath', 'svn异地数据备份路径', '字符型', '100', '', '是', '2014-12-31 15:52:39', '2014-12-31 15:52:39', '启动', ''), ('39', 'propertiesofsvn', '', 'svn_distancekeeptime', '异地备份保留时间', '字符型', '100', '', '是', '2014-12-31 15:53:27', '2014-12-31 15:53:27', '启动', ''), ('40', 'svnresourceslist', '', 'priresoulibname', '一级资源库名', '字符型', '255', '', '是', '2015-01-05 15:59:56', '2015-01-05 15:59:56', '启动', ''), ('41', 'svnresourceslist', '', 'primarysource', '一级资源库路径', '字符型', '255', '', '是', '2015-01-05 16:19:08', '2015-01-05 16:19:08', '启动', ''), ('42', 'svnresourceslist', '', 'size', '一级资源库的大小', '字符型', '100', '', '是', '2015-01-05 16:20:23', '2015-01-05 16:20:23', '启动', ''), ('43', 'svnresourceslist', '', 'pridescription', '一级资源库说明', '字符型', '100', '', '是', '2015-01-05 16:22:21', '2015-01-05 16:22:21', '启动', ''), ('44', 'svnresourceslist', '', 'secresoulibname', '二级资源库名', '字符型', '255', '', '是', '2015-01-05 16:25:37', '2015-01-05 16:25:37', '启动', ''), ('45', 'svnresourceslist', '', 'secdescription', '二级资源库说明', '字符型', '255', '', '是', '2015-01-05 16:26:28', '2015-01-05 16:26:28', '启动', ''), ('46', 'svnresourceslist', '', 'secondsource', '二级资源库路径', '字符型', '255', '', '是', '2015-01-05 16:40:18', '2015-01-05 16:40:18', '启动', ''), ('47', 'checksvnbackup', '', 'check_svnlocalbkinfo', '本地备份文件名及信息', '字符型', '255', '', '是', '2015-01-09 09:49:47', '2015-01-09 09:49:47', '启动', ''), ('49', 'ediipguanli', 'akka端口', 'akka', 'akka端口', '字符型', '100', '', '是', '2015-04-09 19:51:05', '2015-04-09 19:51:05', '启动', ''), ('50', 'ediipguanli', 'ip', 'ip', 'ip', '字符型', '100', '', '是', '2015-04-09 19:53:03', '2015-04-09 19:53:03', '启动', ''), ('51', 'ediipguanli', '保险公司', 'baoxiangongsi', '保险公司', '字符型', '100', '', '是', '2015-04-09 19:53:27', '2015-04-09 19:53:27', '启动', ''), ('52', 'ediipguanli', '环境', '52zzborcom', '环境', '字符型', '100', '', '是', '2015-04-09 19:56:08', '2015-04-09 19:56:08', '启动', ''), ('53', 'jigouguanli', '机构编码', 'code', '机构编码', '字符型', '1000', '', '是', '2015-04-13 15:09:28', '2015-04-13 15:09:28', '启动', ''), ('54', 'jigouguanli', '机构名称', 'name', '机构名称', '字符型', '1000', '', '是', '2015-04-13 15:10:47', '2015-04-13 15:10:47', '启动', ''), ('55', 'jigouguanli', '所属分区', 'fenqu', '所属分区', '字符型', '1000', '', '是', '2015-04-13 15:11:13', '2015-04-13 15:11:13', '启动', ''), ('60', 'jianchakb', '', 'check_man', '检查人', '字符型', '64', '', '是', '2015-06-23 15:09:58', '2015-06-23 15:09:58', '启动', ''), ('61', 'jianchakb', '', 'check_server', '检查的机器', '字符型', '64', '', '是', '2015-06-23 15:12:44', '2015-06-23 15:12:44', '启动', ''), ('62', 'jianchakb', '', 'check_kblocalbkinfo', '本地备份情况', '字符型', '128', '', '是', '2015-06-23 15:13:30', '2015-06-23 15:13:30', '启动', ''), ('63', 'jianchakb', '', 'check_kbbackup', '异地备份情况', '字符型', '128', '', '是', '2015-06-23 15:14:09', '2015-06-23 15:14:09', '启动', ''), ('64', 'jianchakb', '', 'check_info', '检查情况', '字符型', '128', '', '是', '2015-06-23 15:14:38', '2015-06-23 15:14:38', '启动', ''), ('66', 'ldapbeifen', '备份开始时间', 'startdate', '备份开始时间', '字符型', '100', '', '是', '2016-03-25 17:22:45', '2016-03-25 17:22:45', '启动', ''), ('67', 'ldapbeifen', '备份结束时间', 'enddate', '备份结束时间', '字符型', '100', '', '是', '2016-03-25 17:24:06', '2016-03-25 17:24:06', '启动', ''), ('69', 'ldapbeifen', 'ldap备份结果', 'reson', 'ldap备份结果', '字符型', '50', '', '是', '2016-03-25 17:41:13', '2016-03-25 17:41:13', '启动', ''), ('70', 'ldapbeifen', '不成功数', 'caldata', '不成功数', '字符型', '50', '', '是', '2016-03-25 17:43:45', '2016-03-25 17:43:45', '启动', ''), ('72', 'yfworking', '', 'dep', '部门', '字符型', '100', '', '是', '2016-05-09 15:24:44', '2016-05-09 15:24:44', '启动', ''), ('73', 'yfworking', '', 'name', '姓名', '字符型', '100', '', '是', '2016-05-09 15:29:03', '2016-05-09 15:29:03', '启动', ''), ('74', 'yfworking', '', 'sys', '所属系统', '字符型', '100', '', '是', '2016-05-09 15:29:32', '2016-05-09 15:29:32', '启动', ''), ('75', 'yfworking', '', 'func', '负责功能', '字符型', '100', '', '是', '2016-05-09 15:30:12', '2016-05-09 15:30:12', '启动', ''), ('76', 'yfworking', '', 'tel', '联系方式', '字符型', '100', '', '是', '2016-05-09 15:30:41', '2016-05-09 15:30:41', '启动', ''), ('77', 'olddataback', '分区', 'fenqu', '分区', '字符型', '50', '', '是', '2016-10-21 09:52:20', '2016-10-21 09:52:20', '启动', ''), ('78', 'olddataback', '名称', 'mingcheng', '名称', '字符型', '30', '', '是', '2016-10-21 09:53:14', '2016-10-21 09:53:14', '启动', ''), ('79', 'olddataback', '备份机器', 'backmachine', '备份机器', '字符型', '50', '', '是', '2016-10-21 09:53:47', '2016-10-21 09:53:47', '启动', ''), ('80', 'olddataback', '备份方式', 'backstyle', '备份方式', '字符型', '100', '', '是', '2016-10-21 09:54:39', '2016-10-21 09:54:39', '启动', ''), ('81', 'olddataback', '备份路径', 'backpath', '备份路径', '字符型', '100', '', '是', '2016-10-21 09:55:14', '2016-10-21 09:55:14', '启动', '');
COMMIT;

-- ----------------------------
--  Table structure for `pre_article`
-- ----------------------------
DROP TABLE IF EXISTS `pre_article`;
CREATE TABLE `pre_article` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `status` tinyint(1) DEFAULT NULL,
  `create_time` int(10) unsigned DEFAULT NULL,
  `update_time` int(10) unsigned DEFAULT NULL,
  `is_top` tinyint(1) unsigned DEFAULT '0',
  `seokey` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tags` varchar(100) DEFAULT NULL,
  `catid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catstr` varchar(150) DEFAULT '',
  `keywords` varchar(200) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `newtype` tinyint(1) DEFAULT NULL,
  `title_in_keywords` tinyint(1) DEFAULT NULL,
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_keywords` tinyint(1) DEFAULT NULL,
  `seokey_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_title` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `menupos` varchar(20) DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(30) DEFAULT NULL,
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`) USING BTREE,
  KEY `catstr` (`catstr`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_attach`
-- ----------------------------
DROP TABLE IF EXISTS `pre_attach`;
CREATE TABLE `pre_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` varchar(20) NOT NULL,
  `extension` varchar(20) NOT NULL,
  `savepath` varchar(255) NOT NULL,
  `savename` varchar(255) NOT NULL,
  `module` varchar(100) NOT NULL,
  `record_id` int(11) NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `download_count` mediumint(9) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL,
  `pid` int(12) unsigned NOT NULL,
  `sort` int(8) unsigned NOT NULL,
  `version` smallint(3) unsigned NOT NULL,
  `is_dir` tinyint(1) unsigned NOT NULL,
  `remark` varchar(255) NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `verify` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_top` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `module` (`module`) USING BTREE,
  KEY `recordId` (`record_id`) USING BTREE,
  KEY `userId` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_b2cnet`
-- ----------------------------
DROP TABLE IF EXISTS `pre_b2cnet`;
CREATE TABLE `pre_b2cnet` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `gongwang_ip` varchar(64) DEFAULT NULL COMMENT '公网ip',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(64) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(64) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_backtest`
-- ----------------------------
DROP TABLE IF EXISTS `pre_backtest`;
CREATE TABLE `pre_backtest` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `ip` varchar(64) DEFAULT NULL COMMENT 'ip',
  `user` varchar(64) DEFAULT NULL COMMENT '使用者',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_baowangshebei`
-- ----------------------------
DROP TABLE IF EXISTS `pre_baowangshebei`;
CREATE TABLE `pre_baowangshebei` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `snmp` varchar(255) DEFAULT NULL COMMENT 'SNMP监控服务器ip',
  `account` varchar(255) DEFAULT NULL COMMENT '电信光纤账号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `person` varchar(255) DEFAULT NULL COMMENT '光纤联系人',
  `mouse` varchar(255) DEFAULT NULL COMMENT '鼠标',
  `keyboard` varchar(255) DEFAULT NULL COMMENT '键盘',
  `usemouse` varchar(255) DEFAULT NULL COMMENT '鼠标领用记录',
  `usekeyboard` varchar(255) DEFAULT NULL COMMENT '键盘领用记录',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_blog`
-- ----------------------------
DROP TABLE IF EXISTS `pre_blog`;
CREATE TABLE `pre_blog` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `status` tinyint(1) DEFAULT NULL,
  `create_time` int(10) unsigned DEFAULT NULL,
  `update_time` int(10) unsigned DEFAULT NULL,
  `is_top` tinyint(1) unsigned DEFAULT '0',
  `seokey` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tags` varchar(100) DEFAULT NULL,
  `catid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catstr` varchar(150) DEFAULT '',
  `keywords` varchar(200) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `title_in_keywords` tinyint(1) DEFAULT NULL,
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_keywords` tinyint(1) DEFAULT NULL,
  `seokey_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_title` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `menupos` varchar(20) DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(30) DEFAULT NULL,
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`) USING BTREE,
  KEY `catstr` (`catstr`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_bwzzbmonitor`
-- ----------------------------
DROP TABLE IF EXISTS `pre_bwzzbmonitor`;
CREATE TABLE `pre_bwzzbmonitor` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `app_key` varchar(100) DEFAULT NULL COMMENT '应用KEY',
  `app_cpu` varchar(100) DEFAULT NULL COMMENT 'CPU',
  `app_mem` varchar(100) DEFAULT NULL COMMENT '内存',
  `process_num` varchar(100) DEFAULT NULL COMMENT '进程数',
  `app_port` varchar(100) DEFAULT NULL COMMENT '应用端口',
  `app_status` varchar(100) DEFAULT NULL COMMENT '应用url健康',
  `app_disk` varchar(100) DEFAULT NULL COMMENT '磁盘状态',
  `app_logs` varchar(200) DEFAULT NULL COMMENT '应用日志',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_case`
-- ----------------------------
DROP TABLE IF EXISTS `pre_case`;
CREATE TABLE `pre_case` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `content` text,
  `sort` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `member_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `tags` varchar(255) NOT NULL,
  `catid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catstr` varchar(150) DEFAULT '',
  `seokey` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `title_in_keywords` tinyint(1) DEFAULT NULL,
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_keywords` tinyint(1) DEFAULT NULL,
  `seokey_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_title` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `menupos` varchar(20) DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(30) DEFAULT NULL,
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`) USING BTREE,
  KEY `catstr` (`catstr`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_category`
-- ----------------------------
DROP TABLE IF EXISTS `pre_category`;
CREATE TABLE `pre_category` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `sort` smallint(6) unsigned DEFAULT NULL,
  `pid` mediumint(8) unsigned DEFAULT NULL,
  `level` smallint(5) unsigned DEFAULT NULL,
  `module` varchar(25) NOT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `title_in_keywords` tinyint(1) DEFAULT '0',
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `menupos` varchar(20) DEFAULT NULL,
  `counts` mediumint(8) unsigned DEFAULT '0',
  `thumb_on` tinyint(1) DEFAULT '0',
  `water_on` tinyint(1) DEFAULT '0',
  `thumb_width` smallint(5) DEFAULT '0',
  `thumb_height` smallint(5) DEFAULT '0',
  `water_pic` varchar(100) DEFAULT '',
  `water_position` smallint(3) DEFAULT '0',
  `list_tpl` varchar(100) DEFAULT '',
  `content_tpl` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `level` (`level`) USING BTREE,
  KEY `pid` (`pid`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_checksvnbackup`
-- ----------------------------
DROP TABLE IF EXISTS `pre_checksvnbackup`;
CREATE TABLE `pre_checksvnbackup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `check_man` varchar(32) DEFAULT '' COMMENT '检测人',
  `check_server` varchar(64) DEFAULT '' COMMENT '检测哪个svn数据备份',
  `check_svnlocalbkinfo` varchar(255) DEFAULT '' COMMENT '本地备份文件名及大小',
  `check_svnbackup` varchar(100) DEFAULT NULL COMMENT '异地备份文件名及大小',
  `check_info` varchar(100) DEFAULT '' COMMENT '检测信息',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_comConfig`
-- ----------------------------
DROP TABLE IF EXISTS `pre_comConfig`;
CREATE TABLE `pre_comConfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `pro_type` varchar(64) DEFAULT NULL,
  `pro_dir` varchar(100) DEFAULT NULL,
  `java_versoin` varchar(64) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `note` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_comgrant`
-- ----------------------------
DROP TABLE IF EXISTS `pre_comgrant`;
CREATE TABLE `pre_comgrant` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `IP` varchar(64) DEFAULT NULL COMMENT 'ip',
  `mysqlleixing` varchar(64) DEFAULT NULL COMMENT '数据库类型',
  `zhanghao` varchar(64) DEFAULT NULL COMMENT '账号',
  `fuquanadd` varchar(64) DEFAULT NULL COMMENT '授权地址',
  `mysql` varchar(64) DEFAULT NULL COMMENT '数据库',
  `fuquandaxiao` varchar(64) DEFAULT NULL COMMENT '赋权权限',
  `status` varchar(128) DEFAULT NULL COMMENT '使用状态',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_comment`
-- ----------------------------
DROP TABLE IF EXISTS `pre_comment`;
CREATE TABLE `pre_comment` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `sort` mediumint(5) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `modid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `module` varchar(30) DEFAULT '',
  `contents` text,
  `email` varchar(50) DEFAULT NULL,
  `nickname` varchar(30) DEFAULT NULL,
  `isreply` tinyint(1) DEFAULT '0',
  `replycontent` text,
  `dig_good` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `dig_bad` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_commysql`
-- ----------------------------
DROP TABLE IF EXISTS `pre_commysql`;
CREATE TABLE `pre_commysql` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `gongwangip` varchar(64) DEFAULT NULL COMMENT '公网ip',
  `neiwangip` varchar(64) DEFAULT NULL COMMENT '内网ip',
  `user` varchar(64) DEFAULT '开启' COMMENT '用户',
  `shilie` varchar(10) DEFAULT NULL COMMENT '实例',
  `shujku` varchar(128) DEFAULT NULL COMMENT '数据库',
  `yingyongmingcheng` varchar(64) DEFAULT NULL COMMENT '应用名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_config`
-- ----------------------------
DROP TABLE IF EXISTS `pre_config`;
CREATE TABLE `pre_config` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `value` text,
  `create_time` int(11) unsigned NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `name` varchar(30) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `extra` text,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` mediumint(5) unsigned NOT NULL,
  `cg` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_cron_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_cron_manage`;
CREATE TABLE `pre_cron_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `plan_brif` varchar(64) DEFAULT NULL COMMENT '计划任务简介',
  `plan_network_ip` varchar(64) DEFAULT NULL COMMENT '计划任务服务器',
  `plan_content` varchar(64) DEFAULT NULL COMMENT '计划任务内容',
  `plan_goal` varchar(64) DEFAULT NULL COMMENT '计划任务目的',
  `plan_run_start_time` varchar(64) DEFAULT NULL COMMENT '计划任务开始时间',
  `plan_run_end_time` varchar(64) DEFAULT NULL COMMENT '计划任务结束时间',
  `plan_run_stuation` varchar(64) DEFAULT NULL COMMENT '计划任务执行情况',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_db_check`
-- ----------------------------
DROP TABLE IF EXISTS `pre_db_check`;
CREATE TABLE `pre_db_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `check_people` varchar(64) DEFAULT NULL COMMENT '检查人',
  `check_network_ip` varchar(64) DEFAULT NULL COMMENT '检查的服务器',
  `check_content` varchar(64) DEFAULT NULL COMMENT '检查的内容',
  `check_date` varchar(64) DEFAULT NULL COMMENT '检查的日期',
  `check_stuation` varchar(64) DEFAULT NULL COMMENT '检查情况',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_db_checkmysql`
-- ----------------------------
DROP TABLE IF EXISTS `pre_db_checkmysql`;
CREATE TABLE `pre_db_checkmysql` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `check_people` varchar(50) DEFAULT NULL COMMENT '检查人',
  `check_network_ip` varchar(50) DEFAULT NULL COMMENT 'ip',
  `check_content` varchar(50) DEFAULT NULL COMMENT '检查内容',
  `check_date` varchar(50) DEFAULT NULL COMMENT '检查日期',
  `check_size` varchar(50) DEFAULT NULL COMMENT '数据大小',
  `check_stuation` varchar(50) DEFAULT NULL COMMENT '检查情况',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_deploy_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_deploy_manage`;
CREATE TABLE `pre_deploy_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `deploy_sys` varchar(64) DEFAULT NULL COMMENT '发布系统',
  `deploy_key` varchar(64) DEFAULT NULL COMMENT '发布关键词',
  `deploy_version` varchar(64) DEFAULT NULL COMMENT '发布版本',
  `deploy_people` varchar(64) DEFAULT NULL COMMENT '发布人',
  `deploy_ip` varchar(64) CHARACTER SET utf32 DEFAULT NULL COMMENT '内网IP地址',
  `deploy_content` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发布内容',
  `deloy_prodir_before` varchar(64) DEFAULT NULL COMMENT '发布前备份',
  `deloy_configdir_before` varchar(64) DEFAULT NULL COMMENT '发布前配置目录',
  `deloy_prodir_after` varchar(64) DEFAULT NULL COMMENT '发布后工程目录',
  `deloy_configdir_after` varchar(64) DEFAULT NULL COMMENT '发布后配置目录',
  `deloy_size` varchar(64) DEFAULT NULL COMMENT '发布后工程大小',
  `deploy_take` varchar(64) DEFAULT NULL COMMENT '发布花费时间',
  `start_time` datetime DEFAULT NULL COMMENT '发布开始时间',
  `rsync_war_time` datetime DEFAULT NULL COMMENT '传包完成时间',
  `start_app_time` datetime DEFAULT NULL COMMENT '应用开始启动时间',
  `end_time` datetime DEFAULT NULL COMMENT '发布结束时间',
  `randomStr` varchar(128) DEFAULT NULL COMMENT '随机字符串',
  `rollRecord` int(64) DEFAULT '0' COMMENT '是否是回滚的记录',
  `rollStatus` int(64) DEFAULT '0' COMMENT '回滚状态',
  `rollbackNumber` int(64) DEFAULT '0' COMMENT '回滚次数',
  `processNumber` int(64) DEFAULT NULL COMMENT '进程数量',
  `status` int(64) DEFAULT '0' COMMENT '发布状态',
  `deloy_prodir_real_before` varchar(64) DEFAULT NULL COMMENT '发布前原始的目录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_deploy_status`
-- ----------------------------
DROP TABLE IF EXISTS `pre_deploy_status`;
CREATE TABLE `pre_deploy_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `deploy_sys` varchar(64) DEFAULT NULL COMMENT '发布系统',
  `deploy_key` varchar(64) DEFAULT NULL COMMENT '发布关键词',
  `deploy_version` varchar(64) DEFAULT NULL COMMENT '发布版本',
  `deploy_people` varchar(64) DEFAULT NULL COMMENT '发布人',
  `deploy_ip` varchar(64) DEFAULT NULL COMMENT '要发布的机器IP',
  `deploy_content` varchar(200) DEFAULT NULL COMMENT '发布内容',
  `deloy_prodir_before` varchar(64) DEFAULT NULL COMMENT '发布前工程目录',
  `deloy_configdir_before` varchar(64) DEFAULT NULL COMMENT '发布前配置目录',
  `deloy_prodir_after` varchar(64) DEFAULT NULL COMMENT '发布后工程目录',
  `deloy_configdir_after` varchar(64) DEFAULT NULL COMMENT '发布后配置目录',
  `deploy_take` varchar(64) DEFAULT NULL COMMENT '发布花费时间',
  `start_time` datetime DEFAULT NULL COMMENT '发布开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '发布结束时间',
  `status` int(64) DEFAULT '0' COMMENT '发布状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_dns_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_dns_manage`;
CREATE TABLE `pre_dns_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `site` varchar(64) NOT NULL COMMENT '主域名',
  `use` varchar(128) DEFAULT NULL COMMENT '主域名用途',
  `second_site` varchar(15) NOT NULL COMMENT '子域名',
  `owner` varchar(64) DEFAULT NULL COMMENT '账号所有者',
  `line_type` varchar(64) DEFAULT NULL COMMENT '线路类型',
  `site_network` varchar(64) DEFAULT NULL COMMENT '主域名主机地址',
  `site_use` varchar(64) DEFAULT NULL COMMENT '子域名用途',
  `hetong_num` varchar(64) DEFAULT NULL COMMENT '合同编号',
  `hetong_name` varchar(64) DEFAULT NULL COMMENT '合同名称',
  `hetong_product` varchar(64) DEFAULT NULL COMMENT '合同产品',
  `hetong_expire` varchar(64) DEFAULT NULL COMMENT '合同有效期',
  `hetong_take` varchar(64) DEFAULT NULL COMMENT '合同费用',
  `zhuce_address` varchar(64) DEFAULT NULL COMMENT '注册地址',
  `zhuce_account` varchar(64) DEFAULT NULL COMMENT '账号',
  `expire` varchar(64) DEFAULT NULL COMMENT '账号有效期',
  `zhuce_passwd` varchar(64) DEFAULT NULL COMMENT '密码',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_down`
-- ----------------------------
DROP TABLE IF EXISTS `pre_down`;
CREATE TABLE `pre_down` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `content` text,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0',
  `count` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `catid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catstr` varchar(150) DEFAULT '',
  `filename` varchar(255) DEFAULT NULL,
  `size` varchar(20) NOT NULL,
  `extension` varchar(20) NOT NULL,
  `download_count` mediumint(9) unsigned NOT NULL DEFAULT '0',
  `pic` varchar(150) DEFAULT '',
  `attachfile` varchar(150) DEFAULT '',
  `seokey` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `title_in_keywords` tinyint(1) DEFAULT NULL,
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_keywords` tinyint(1) DEFAULT NULL,
  `seokey_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_title` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `menupos` varchar(20) DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(30) DEFAULT NULL,
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`) USING BTREE,
  KEY `catstr` (`catstr`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_drop_down`
-- ----------------------------
DROP TABLE IF EXISTS `pre_drop_down`;
CREATE TABLE `pre_drop_down` (
  `id` int(11) NOT NULL COMMENT '下拉框的字段对应的id',
  `main_string` varchar(64) NOT NULL COMMENT '下拉框的对应的字段名称',
  `main_drop_content` varchar(128) DEFAULT NULL COMMENT '下拉框对应的下拉内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `pre_drop_down`
-- ----------------------------
BEGIN;
INSERT INTO `pre_drop_down` VALUES ('1', 'status', '启动,关闭'), ('1', 'equipment_place', '8楼,9楼,10楼,11楼,12楼,13楼');
COMMIT;

-- ----------------------------
--  Table structure for `pre_dropdown`
-- ----------------------------
DROP TABLE IF EXISTS `pre_dropdown`;
CREATE TABLE `pre_dropdown` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `id_key` int(15) NOT NULL COMMENT '下拉框的字段对应的id',
  `main_string` varchar(64) DEFAULT NULL COMMENT '下拉框的对应的字段名称',
  `main_drop_content` varchar(128) DEFAULT NULL COMMENT '下拉框对应的下拉内容',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `pre_dropdown`
-- ----------------------------
BEGIN;
INSERT INTO `pre_dropdown` VALUES ('1', '1', 'status', '启动,关闭', '2014-12-05 17:40:13', '2014-12-05 17:40:13', '开启', ''), ('2', '1', 'equipment_place', '8楼,9楼,10楼,11楼,12楼,13楼', '2014-12-08 11:37:51', '2014-12-08 11:37:51', '开启', ''), ('3', '3', 'is_success', '否,是', '2014-12-08 14:33:44', '2014-12-08 14:33:48', '开启', ''), ('4', '3', 'status', '启动,关闭', '2014-12-08 14:33:10', '2014-12-08 14:33:14', '开启', null), ('5', '3', 'str_type', '字符型,整型,日期时间型', '2014-12-08 14:32:29', '2014-12-08 14:32:33', '开启', null), ('96', '79', 'sys', 'CM,EDI,精灵,渠道,工作流,规则,前端,数据库', '2016-05-13 15:56:28', '2016-05-13 15:56:28', '', ''), ('97', '79', 'status', '已提交至SVN,已更新至uat,已发包,uat测试中,uat测试未通过,uat测试通过,已作废,提取不到,只发了生产,报错未发', '2016-05-13 15:58:23', '2016-05-13 15:58:23', '', ''), ('98', '79', 'sta_1', '未发包,已发包', '2016-05-13 16:02:41', '2016-05-13 16:02:41', '', ''), ('99', '79', 'sta_2', '未发包,已发包', '2016-05-13 16:02:59', '2016-05-13 16:02:59', '', ''), ('100', '79', 'yn', '不发生产,发生产', '2016-05-13 16:03:41', '2016-05-13 16:03:41', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `pre_edi`
-- ----------------------------
DROP TABLE IF EXISTS `pre_edi`;
CREATE TABLE `pre_edi` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_ediipguanli`
-- ----------------------------
DROP TABLE IF EXISTS `pre_ediipguanli`;
CREATE TABLE `pre_ediipguanli` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `52zzborcom` varchar(100) DEFAULT '' COMMENT '环境',
  `baoxiangongsi` varchar(100) DEFAULT '' COMMENT '保险公司',
  `ip` varchar(100) DEFAULT '' COMMENT 'ip',
  `edi` varchar(100) DEFAULT '' COMMENT 'edi端口',
  `akka` varchar(100) DEFAULT '' COMMENT 'akka端口',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_email_check`
-- ----------------------------
DROP TABLE IF EXISTS `pre_email_check`;
CREATE TABLE `pre_email_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `check_people` varchar(64) DEFAULT NULL COMMENT '值班人',
  `check_network_ip` varchar(64) DEFAULT NULL COMMENT '涉及服务器',
  `check_content` varchar(64) DEFAULT NULL COMMENT '主要问题',
  `check_week` varchar(64) DEFAULT NULL COMMENT '第几周',
  `check_date` varchar(64) DEFAULT NULL COMMENT '发生故障日期',
  `check_stuation` varchar(64) DEFAULT NULL COMMENT '具体情况',
  `check_desc` varchar(64) DEFAULT NULL COMMENT '详细描述',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '关闭' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_equipment`
-- ----------------------------
DROP TABLE IF EXISTS `pre_equipment`;
CREATE TABLE `pre_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `equipment_name` varchar(64) DEFAULT NULL COMMENT '设备名称',
  `equipment_type` varchar(64) DEFAULT NULL COMMENT '设备类型',
  `admin_account` varchar(64) DEFAULT NULL COMMENT '设备管理账号',
  `admin_passwd` varchar(64) DEFAULT NULL COMMENT '设备管理密码',
  `name` varchar(64) NOT NULL COMMENT '无线名称',
  `password` varchar(128) DEFAULT NULL COMMENT '无线密码',
  `sign` int(11) DEFAULT NULL COMMENT '信号频段',
  `network_ip` varchar(15) NOT NULL COMMENT '管理地址',
  `equipment_place` varchar(64) DEFAULT NULL COMMENT '设备位置',
  `equipment_error_time` varchar(64) DEFAULT NULL COMMENT '设备故障记录时间',
  `owner` varchar(64) DEFAULT NULL COMMENT '所有者',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `pro_dir` varchar(100) DEFAULT '' COMMENT '工程目录',
  `check_info` varchar(100) DEFAULT '' COMMENT '检测信息',
  `gongsi` varchar(100) DEFAULT '' COMMENT '保险公司',
  `baoxiangongsi` varchar(100) DEFAULT '' COMMENT '保险公司',
  `number` varchar(50) DEFAULT '' COMMENT 'ldap测验返回的个数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_error_record`
-- ----------------------------
DROP TABLE IF EXISTS `pre_error_record`;
CREATE TABLE `pre_error_record` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `processors` varchar(64) DEFAULT NULL COMMENT '处理人',
  `app_name` varchar(100) DEFAULT NULL COMMENT '应用名称',
  `network_ip` varchar(25) DEFAULT NULL COMMENT '内网IP',
  `error_brief` varchar(64) DEFAULT NULL COMMENT '故障简介',
  `error_type` varchar(64) DEFAULT NULL COMMENT '故障类型',
  `error_reason` varchar(128) DEFAULT NULL COMMENT '故障原因',
  `deal_ways` varchar(128) DEFAULT NULL COMMENT '处理方法',
  `restore_situation` varchar(28) DEFAULT NULL COMMENT '恢复情况',
  `affect_time` varchar(64) DEFAULT NULL COMMENT '影响时间',
  `malfuction_level` varchar(64) DEFAULT NULL COMMENT '故障等级',
  `description` text COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新记录时间',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_fault52zzb`
-- ----------------------------
DROP TABLE IF EXISTS `pre_fault52zzb`;
CREATE TABLE `pre_fault52zzb` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `dealwith_man` varchar(255) DEFAULT NULL COMMENT '处理人',
  `application` varchar(255) DEFAULT NULL COMMENT '应用类型',
  `fault_desc` varchar(255) DEFAULT NULL COMMENT '故障简介',
  `fault_type` varchar(255) DEFAULT NULL COMMENT '故障类型',
  `recovery` varchar(255) DEFAULT NULL COMMENT '恢复情况',
  `influence_time` varchar(255) DEFAULT NULL COMMENT '影响时间',
  `fault_level` varchar(255) DEFAULT NULL COMMENT '故障等级',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_financialcloud`
-- ----------------------------
DROP TABLE IF EXISTS `pre_financialcloud`;
CREATE TABLE `pre_financialcloud` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(128) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `gongwang_ip` varchar(64) DEFAULT NULL COMMENT '公网ip',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(64) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(64) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_focuspattern`
-- ----------------------------
DROP TABLE IF EXISTS `pre_focuspattern`;
CREATE TABLE `pre_focuspattern` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) DEFAULT NULL,
  `focuscode` varchar(100) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `fpattern` varchar(100) DEFAULT NULL,
  `fsecond` smallint(6) DEFAULT NULL,
  `fwidth` smallint(6) DEFAULT NULL,
  `fheight` smallint(6) DEFAULT NULL,
  `fcount` smallint(6) DEFAULT NULL,
  `cachetime` smallint(6) DEFAULT NULL,
  `titleheight` varchar(15) DEFAULT 'default',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_focuspic`
-- ----------------------------
DROP TABLE IF EXISTS `pre_focuspic`;
CREATE TABLE `pre_focuspic` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) DEFAULT NULL,
  `focuscode` varchar(100) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `pic` varchar(150) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `remark` text,
  `sort` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `url` varchar(100) DEFAULT NULL,
  `start_time` int(11) unsigned DEFAULT NULL,
  `end_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_host_net`
-- ----------------------------
DROP TABLE IF EXISTS `pre_host_net`;
CREATE TABLE `pre_host_net` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(64) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(64) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_host_org`
-- ----------------------------
DROP TABLE IF EXISTS `pre_host_org`;
CREATE TABLE `pre_host_org` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(64) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(64) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_host_zzb`
-- ----------------------------
DROP TABLE IF EXISTS `pre_host_zzb`;
CREATE TABLE `pre_host_zzb` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `groupby` varchar(128) DEFAULT NULL COMMENT '分组名称',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(164) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(164) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(164) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(100) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  `local_name` varchar(200) DEFAULT NULL COMMENT '识别名称',
  `app_key` varchar(200) DEFAULT NULL COMMENT '应用唯一关键词',
  `is_deloy_dir` varchar(64) DEFAULT NULL COMMENT '发布目录判断',
  `container_dir` varchar(100) DEFAULT NULL COMMENT '容器目录',
  `java_versoin` varchar(64) DEFAULT NULL COMMENT 'java版本',
  `java_home` varchar(100) DEFAULT NULL COMMENT 'java的Home目录',
  `java_confirm` varchar(64) DEFAULT NULL COMMENT 'java版本确认',
  `auto_deloy` varchar(64) DEFAULT '0' COMMENT '是否加入自动发布',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_host_zzb_20150518`
-- ----------------------------
DROP TABLE IF EXISTS `pre_host_zzb_20150518`;
CREATE TABLE `pre_host_zzb_20150518` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(164) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(164) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(164) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(100) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  `local_name` varchar(200) DEFAULT NULL COMMENT '识别名称',
  `app_key` varchar(200) DEFAULT NULL COMMENT '应用唯一关键词',
  `is_deloy_dir` varchar(64) DEFAULT NULL COMMENT '发布目录判断',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_host_zzb_copy`
-- ----------------------------
DROP TABLE IF EXISTS `pre_host_zzb_copy`;
CREATE TABLE `pre_host_zzb_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(164) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(164) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(164) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(100) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  `local_name` varchar(200) DEFAULT NULL COMMENT '识别名称',
  `app_key` varchar(200) DEFAULT NULL COMMENT '应用唯一关键词',
  `is_deloy_dir` varchar(64) DEFAULT NULL COMMENT '发布目录判断',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_ipedi`
-- ----------------------------
DROP TABLE IF EXISTS `pre_ipedi`;
CREATE TABLE `pre_ipedi` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_ipmac`
-- ----------------------------
DROP TABLE IF EXISTS `pre_ipmac`;
CREATE TABLE `pre_ipmac` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `ip` varchar(255) DEFAULT NULL COMMENT 'ip',
  `mac` varchar(255) DEFAULT NULL COMMENT 'mac地址',
  `person` varchar(255) DEFAULT NULL COMMENT '使用人',
  `status` varchar(255) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_jianchakb`
-- ----------------------------
DROP TABLE IF EXISTS `pre_jianchakb`;
CREATE TABLE `pre_jianchakb` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `check_man` varchar(64) DEFAULT NULL COMMENT '检查人',
  `check_server` varchar(64) DEFAULT NULL COMMENT '检查的机器',
  `check_kblocalbkinfo` varchar(128) DEFAULT '开启' COMMENT '本地备份情况',
  `check_kbbackup` varchar(128) DEFAULT NULL COMMENT '异地备份情况',
  `check_info` varchar(128) DEFAULT NULL COMMENT '检查情况',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_jichu`
-- ----------------------------
DROP TABLE IF EXISTS `pre_jichu`;
CREATE TABLE `pre_jichu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_jigoufenqu`
-- ----------------------------
DROP TABLE IF EXISTS `pre_jigoufenqu`;
CREATE TABLE `pre_jigoufenqu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_jigouguanli`
-- ----------------------------
DROP TABLE IF EXISTS `pre_jigouguanli`;
CREATE TABLE `pre_jigouguanli` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `fenqu` varchar(1000) DEFAULT '' COMMENT '所属分区',
  `name` varchar(1000) DEFAULT '' COMMENT '机构名称',
  `code` varchar(1000) DEFAULT '' COMMENT '机构编码',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_jingling`
-- ----------------------------
DROP TABLE IF EXISTS `pre_jingling`;
CREATE TABLE `pre_jingling` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `addr` varchar(100) DEFAULT '' COMMENT '服务器所在地区',
  `company` varchar(100) DEFAULT '' COMMENT '保险公司',
  `type` varchar(100) DEFAULT '' COMMENT '服务器类型',
  `pub_ip` varchar(100) DEFAULT '' COMMENT '公网ip',
  `pri_ip` varchar(100) DEFAULT '' COMMENT '内网ip',
  `key_1` varchar(100) DEFAULT '否' COMMENT '是否需要key',
  `key_2` varchar(100) DEFAULT '否' COMMENT '是否已插key',
  `line` varchar(100) DEFAULT '否' COMMENT '是否专线',
  `vpn` varchar(100) DEFAULT '是' COMMENT '是否需要vpn',
  `ava` varchar(100) DEFAULT '是' COMMENT '是否可以启用',
  `res` varchar(100) DEFAULT '是' COMMENT '未启用原因',
  `state_now` varchar(100) DEFAULT '' COMMENT '当前机器状态',
  `user_x` varchar(100) DEFAULT '' COMMENT '账号',
  `passwd_x` varchar(100) DEFAULT '' COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_job`
-- ----------------------------
DROP TABLE IF EXISTS `pre_job`;
CREATE TABLE `pre_job` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `money` varchar(100) DEFAULT NULL,
  `sort` mediumint(5) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL,
  `start_time` int(11) unsigned NOT NULL DEFAULT '0',
  `end_time` int(11) unsigned NOT NULL DEFAULT '0',
  `needcount` smallint(5) DEFAULT '0',
  `content` text,
  `catid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catstr` varchar(150) DEFAULT '',
  `seokey` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `degree` varchar(100) DEFAULT NULL,
  `jobaddress` varchar(100) DEFAULT NULL,
  `workage` varchar(100) DEFAULT NULL,
  `major` varchar(100) DEFAULT NULL,
  `title_in_keywords` tinyint(1) DEFAULT NULL,
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_keywords` tinyint(1) DEFAULT NULL,
  `seokey_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_title` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(30) DEFAULT NULL,
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`) USING BTREE,
  KEY `catstr` (`catstr`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_kbcheck`
-- ----------------------------
DROP TABLE IF EXISTS `pre_kbcheck`;
CREATE TABLE `pre_kbcheck` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `check_man` varchar(64) DEFAULT NULL COMMENT '检查人',
  `check_server` varchar(64) DEFAULT NULL COMMENT '检查的机器',
  `check_kblocalbkinfo` varchar(128) DEFAULT '开启' COMMENT '本地备份情况',
  `check_kbbackup` varchar(128) DEFAULT NULL COMMENT '异地备份情况',
  `check_info` varchar(128) DEFAULT NULL COMMENT '检查情况',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_ldap`
-- ----------------------------
DROP TABLE IF EXISTS `pre_ldap`;
CREATE TABLE `pre_ldap` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_ldap_net`
-- ----------------------------
DROP TABLE IF EXISTS `pre_ldap_net`;
CREATE TABLE `pre_ldap_net` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `backup` varchar(1000) DEFAULT '' COMMENT 'ldap备份检查',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_ldapbeifen`
-- ----------------------------
DROP TABLE IF EXISTS `pre_ldapbeifen`;
CREATE TABLE `pre_ldapbeifen` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `startdate` varchar(100) DEFAULT '' COMMENT '备份开始时间',
  `enddate` varchar(100) DEFAULT '' COMMENT '备份结束时间',
  `caldata` varchar(50) DEFAULT '' COMMENT '不成功数',
  `reson` varchar(50) DEFAULT '' COMMENT 'ldap备份结果',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_ldapcom`
-- ----------------------------
DROP TABLE IF EXISTS `pre_ldapcom`;
CREATE TABLE `pre_ldapcom` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_link`
-- ----------------------------
DROP TABLE IF EXISTS `pre_link`;
CREATE TABLE `pre_link` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `sort` mediumint(5) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL,
  `isindex` tinyint(1) DEFAULT NULL,
  `baidu_count` mediumint(8) unsigned DEFAULT '0',
  `baidu_index` tinyint(1) DEFAULT '0',
  `linktype` tinyint(1) DEFAULT '0',
  `linkurl` varchar(255) DEFAULT NULL,
  `linktext` varchar(50) DEFAULT NULL,
  `update_date` int(11) unsigned NOT NULL DEFAULT '0',
  `google_pr` smallint(3) DEFAULT '0',
  `baidu_br` smallint(3) DEFAULT '0',
  `baidu_date` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_load_check`
-- ----------------------------
DROP TABLE IF EXISTS `pre_load_check`;
CREATE TABLE `pre_load_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `check_people` varchar(64) DEFAULT NULL COMMENT '检查人',
  `check_network_ip` varchar(64) DEFAULT NULL COMMENT '检查的服务器',
  `check_content` varchar(64) DEFAULT NULL COMMENT '检查的内容',
  `check_week` varchar(64) DEFAULT NULL COMMENT '检查周',
  `check_date` varchar(64) DEFAULT NULL COMMENT '检查的日期',
  `check_stuation` varchar(64) DEFAULT NULL COMMENT '检查情况',
  `check_desc` varchar(64) DEFAULT NULL COMMENT '检查的详细内容',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_log_check`
-- ----------------------------
DROP TABLE IF EXISTS `pre_log_check`;
CREATE TABLE `pre_log_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `check_people` varchar(64) DEFAULT NULL COMMENT '检查人',
  `check_network_ip` varchar(64) DEFAULT NULL COMMENT '检查的服务器',
  `check_content` varchar(64) DEFAULT NULL COMMENT '检查的内容',
  `check_week` varchar(64) DEFAULT NULL COMMENT '检查周',
  `check_date` varchar(64) DEFAULT NULL COMMENT '检查的日期',
  `check_stuation` varchar(64) DEFAULT NULL COMMENT '检查情况',
  `check_desc` varchar(64) DEFAULT NULL COMMENT '检查的详细内容',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_menu`
-- ----------------------------
DROP TABLE IF EXISTS `pre_menu`;
CREATE TABLE `pre_menu` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(25) DEFAULT NULL,
  `name` varchar(25) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `sort` mediumint(5) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `pid` mediumint(5) unsigned DEFAULT NULL,
  `level` tinyint(1) unsigned DEFAULT NULL,
  `target` varchar(15) DEFAULT NULL,
  `position` varchar(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `modid` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_menus`
-- ----------------------------
DROP TABLE IF EXISTS `pre_menus`;
CREATE TABLE `pre_menus` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `title` varchar(25) DEFAULT NULL COMMENT '标题',
  `name` varchar(25) DEFAULT NULL COMMENT '模块名',
  `link` varchar(255) DEFAULT NULL COMMENT '超链接',
  `sort` mediumint(5) unsigned DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `pid` mediumint(5) unsigned DEFAULT NULL COMMENT 'pid',
  `level` tinyint(1) unsigned DEFAULT NULL COMMENT '菜单级别',
  `target` varchar(15) DEFAULT NULL COMMENT '超链接打开方式',
  `display` varchar(100) DEFAULT NULL COMMENT '点击时是否激活',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `pre_menus`
-- ----------------------------
BEGIN;
INSERT INTO `pre_menus` VALUES ('1', '主机管理', '', '', '1', '1', '0', '1', '', 'none', null), ('2', '账号管理', '', '', '2', '1', '0', '1', '', 'none', null), ('3', '自动化管理', '', '', '3', '1', '0', '1', '', 'none', null), ('4', '监控管理', '', '', '4', '1', '0', '1', '', 'none', null), ('5', '故障管理', '', '', '5', '1', '0', '1', '', 'none', null), ('6', '巡检管理', '', '', '6', '1', '0', '1', '', 'none', ''), ('7', '模块管理', '', '', '7', '1', '0', '1', '', 'none', null), ('9', 'org &amp; 环境', 'HostOrg', null, '2', '1', '1', '2', null, 'none', null), ('10', 'uat &amp; 环境', 'HostZzb', null, '3', '1', '1', '2', '', 'none', null), ('11', 'com &amp; 环境', 'ServerDetail', null, '4', '1', '1', '2', null, 'none', null), ('12', 'Svn账号管理', 'SvnManage', null, '1', '1', '2', '2', null, 'none', null), ('13', 'Vpn账号管理', 'VpnManage', null, '2', '1', '2', '2', null, 'none', null), ('8', 'net &amp; 环境', 'HostNet', null, '1', '1', '1', '2', null, 'none', null), ('14', 'Linux系统账号管理', 'SysManage', null, '3', '1', '2', '2', null, 'none', null), ('15', 'Mysql账号列表', 'Mysql', null, '4', '1', '2', '2', null, 'none', null), ('16', 'Mysql账号申请', 'MysqlManage', null, '5', '1', '2', '2', null, 'none', null), ('17', 'Dns账号管理', 'DnsManage', null, '6', '1', '2', '2', null, 'none', null), ('18', '内网账号管理', 'NeiManage', null, '7', '1', '2', '2', null, 'none', null), ('19', '其他账号管理', 'OtherManage', null, '8', '1', '2', '2', null, 'none', null), ('20', '自动化脚本管理', 'ScriptsManage', null, '1', '1', '3', '2', null, 'none', null), ('21', '自动化发布管理', 'Deploy_manage', '', '2', '1', '3', '2', '', 'none', ''), ('22', '自动化任务管理', 'CronManage', null, '3', '1', '3', '2', null, 'none', null), ('23', '故障处理流程', 'ProcessManage', null, '1', '1', '5', '2', null, 'none', null), ('24', '故障详情记录', 'ErrorRecord', null, '2', '1', '5', '2', null, 'none', null), ('25', '系统负载检查记录', 'LoadCheck', null, '1', '1', '6', '2', null, 'none', null), ('26', '系统错误日志检查记录', 'LogCheck', null, '2', '1', '6', '2', null, 'none', null), ('27', '数据备份检查记录', 'DbCheck', null, '3', '1', '6', '2', null, 'none', null), ('28', '邮件问题处理记录', 'EmailCheck', null, '4', '1', '6', '2', null, 'none', null), ('29', '模块信息管理', 'AddModule', null, '1', '1', '7', '2', null, 'none', null), ('30', '服务器和应用健康监控', null, '', '1', '1', '4', '2', '_blank', 'none', null), ('31', '服务器连接数监控', '', 'Admin/Echarts/connect', '2', '1', '4', '2', '_blank', 'none', null), ('32', '业务数据监控', 'Echarts', null, '3', '1', '4', '2', '_blank', 'none', null), ('34', '下拉框管理', 'Dropdown', '', '3', '1', '7', '2', '', 'none', ''), ('33', '字段管理', 'Addstr', '', '2', '1', '7', '2', '', 'active', ''), ('35', '数据表管理', 'Tbmanage', '', '4', '1', '7', '2', '', 'none', ''), ('36', '菜单管理', 'Menus', '', '5', '1', '7', '2', '', 'none', ''), ('37', 'test-net环境', 'B2cnet', '', '5', '1', '1', '2', '', 'none', ''), ('38', '文档管理', '', '', '8', '1', '0', '1', '', 'none', ''), ('39', '模块添加说明', '', 'instructions.html', '9', '1', '38', '2', '', 'none', ''), ('42', 'svn备份检查', 'Checksvnbackup', '', '7', '1', '6', '2', '', 'none', ''), ('41', '备用主机', 'Unusedcomputer', '', '7', '1', '1', '2', '', 'none', ''), ('49', '仓库pc机管理', 'Pc', '', '11', '1', '1', '2', '', 'none', ''), ('47', 'org 新框架 & 环境', 'Newframework', '', '9', '1', '1', '2', '', 'none', ''), ('45', 'SVN数据备份基本信息', 'Propertiesofsvn', '', '6', '1', '1', '2', '', 'none', ''), ('46', 'SVN资源库列表', 'Svnresourceslist', '', '8', '1', '1', '2', '', 'none', ''), ('48', '机房物理服务器列表', 'Xenserverlist', '', '10', '1', '1', '2', '', 'none', ''), ('50', '预生产故障处理记录表', 'Fault52zzb', '', '3', '1', '5', '2', '', 'none', ''), ('56', '机构管理', 'Jigouguanli', '', '18', '1', '1', '2', '', 'none', ''), ('53', '端口管理', 'Ediipguanli', '', '16', '1', '1', '2', '', 'none', ''), ('57', '保网SNMP、设备管理', 'Baowangshebei', '', '4', '1', '4', '2', '', 'none', ''), ('58', '无线信号监控', 'Wuxianjiankong', '', '5', '1', '4', '2', '', 'none', ''), ('59', '生产数据库使用情况', 'Commysql', '', '9', '1', '2', '2', '', 'none', ''), ('61', '生产数据库授权列表', 'Comgrant', '', '10', '1', '2', '2', '', 'none', ''), ('69', '备份检查', 'Jianchakb', '', '6', '1', '6', '2', '', 'none', ''), ('63', '预生产应用监控管理', 'Bwzzbmonitor', '', '10', '1', '4', '2', '', 'none', ''), ('71', '测试备份机器', 'Backtest', '', '15', '1', '1', '2', '', 'none', ''), ('70', 'ip-mac-person对应表', 'Ipmac', '', '11', '1', '2', '2', '', 'none', ''), ('72', 'test-org环境', 'Qunabaoorg', '', '1', '1', '1', '2', '', 'none', ''), ('74', 'redis&mongodb信息', 'Rm_shuju', '', '1', '1', '1', '2', '', 'none', ''), ('73', 'test-com环境', 'Qunabaocom', '', '1', '1', '1', '2', '', 'none', ''), ('78', 'mysql备份情况', 'Db_checkmysql', '', '10', '1', '6', '2', '', 'none', ''), ('77', 'ldap备份检查', 'Ldapbeifen', '', '9', '1', '6', '2', '', 'none', ''), ('88', '旧数据保留情况', 'Olddataback', '', '100', '1', '1', '2', '', 'none', ''), ('79', '云环境', 'Financialcloud', '', '79', '1', '1', '2', '', 'none', ''), ('80', '新业务生产', 'Xinkuangjia_com', '', '81', '1', '1', '2', '', 'none', ''), ('83', '研发人员清单', 'Yanfarecord', '', '11', '1', '4', '2', '', 'none', ''), ('87', '自动化发布状态', 'Deploy_status', '', '5', '1', '3', '2', '', 'none', ''), ('85', '新业务2服务器', 'Roboot', '', '83', '1', '1', '2', '', 'none', ''), ('86', '升级管理列表', 'Svn_uplist', '', '6', '1', '3', '2', '', 'none', '');
COMMIT;

-- ----------------------------
--  Table structure for `pre_menus_bak`
-- ----------------------------
DROP TABLE IF EXISTS `pre_menus_bak`;
CREATE TABLE `pre_menus_bak` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '序号',
  `title` varchar(25) DEFAULT NULL COMMENT '标题',
  `name` varchar(25) DEFAULT NULL COMMENT '模块名',
  `link` varchar(255) DEFAULT NULL COMMENT '链接',
  `ulinkrl` varchar(255) DEFAULT NULL COMMENT '超链接',
  `sort` mediumint(5) unsigned DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `pid` mediumint(5) unsigned DEFAULT NULL COMMENT 'pid',
  `level` tinyint(1) unsigned DEFAULT NULL COMMENT '菜单级别',
  `target` varchar(15) DEFAULT NULL COMMENT '超链接打开方式',
  `display` varchar(100) DEFAULT NULL COMMENT '点击时是否激活',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_mysql`
-- ----------------------------
DROP TABLE IF EXISTS `pre_mysql`;
CREATE TABLE `pre_mysql` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(64) NOT NULL COMMENT '名称',
  `network_ip` varchar(25) NOT NULL COMMENT '数据库地址',
  `type` varchar(64) DEFAULT NULL COMMENT '实例类型',
  `charset` varchar(28) DEFAULT NULL COMMENT '默认字符集',
  `disk` varchar(64) DEFAULT NULL COMMENT '容量',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录时间',
  `root_passwd` varchar(64) DEFAULT NULL COMMENT 'root密码',
  `insconn_passwd` varchar(64) DEFAULT NULL COMMENT 'insconn密码',
  `map_server` varchar(64) DEFAULT NULL COMMENT '映射机器',
  `map_network` varchar(15) DEFAULT NULL COMMENT '映射内网地址',
  `map_port` varchar(10) DEFAULT NULL COMMENT '映射端口',
  `note` varchar(64) DEFAULT NULL COMMENT '备注',
  `status` varchar(64) DEFAULT '开启' COMMENT '状态',
  `mark` int(25) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_mysql_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_mysql_manage`;
CREATE TABLE `pre_mysql_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `apply_server_name` varchar(64) DEFAULT NULL COMMENT '申请名称',
  `apply_server_purpose` varchar(128) DEFAULT NULL COMMENT '申请的目的',
  `apply_people` varchar(64) DEFAULT NULL COMMENT '申请人',
  `isslave` varchar(64) DEFAULT NULL COMMENT '是否是从数据库',
  `apply_server_ip` varchar(15) DEFAULT NULL COMMENT '数据库IP',
  `apply_database` varchar(64) DEFAULT NULL COMMENT '数据库名',
  `apply_pri` varchar(128) DEFAULT '' COMMENT '申请权限',
  `apply_account` varchar(64) DEFAULT '' COMMENT '申请账号',
  `apply_passwd` varchar(64) DEFAULT '' COMMENT '申请密码',
  `maping_status` varchar(64) DEFAULT NULL COMMENT '是否开启映射',
  `maping_external_ip` varchar(15) DEFAULT NULL COMMENT '映射服务器外网IP',
  `maping_network_ip` varchar(15) DEFAULT NULL COMMENT '映射服务器内网IP',
  `maping_port` varchar(28) DEFAULT NULL COMMENT '映射服务器端口',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_nei_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_nei_manage`;
CREATE TABLE `pre_nei_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `account` varchar(64) NOT NULL COMMENT '账号',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `network_ip` varchar(15) NOT NULL COMMENT '服务器地址',
  `owner` varchar(64) DEFAULT NULL COMMENT '账号所有者',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `account_pri` varchar(64) DEFAULT NULL COMMENT '账号的权限',
  `account_expire` varchar(64) DEFAULT NULL COMMENT '账号有效期',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_new`
-- ----------------------------
DROP TABLE IF EXISTS `pre_new`;
CREATE TABLE `pre_new` (
  `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `status` tinyint(1) DEFAULT NULL,
  `create_time` int(10) unsigned DEFAULT NULL,
  `update_time` int(10) unsigned DEFAULT NULL,
  `is_top` tinyint(1) unsigned DEFAULT '0',
  `seokey` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tags` varchar(100) DEFAULT NULL,
  `catid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catstr` varchar(150) DEFAULT '',
  `keywords` varchar(200) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `newtype` tinyint(1) DEFAULT NULL,
  `title_in_keywords` tinyint(1) DEFAULT NULL,
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_keywords` tinyint(1) DEFAULT NULL,
  `seokey_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_title` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `menupos` varchar(20) DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(30) DEFAULT NULL,
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`) USING BTREE,
  KEY `catstr` (`catstr`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_newframework`
-- ----------------------------
DROP TABLE IF EXISTS `pre_newframework`;
CREATE TABLE `pre_newframework` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(255) DEFAULT NULL COMMENT '分区名称',
  `server_name` varchar(255) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(64) DEFAULT NULL COMMENT '内网ip',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'cpu',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(160) DEFAULT NULL COMMENT '应用类型',
  `config_dir` varchar(255) DEFAULT NULL COMMENT '配置目录',
  `pro_dir` varchar(255) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(255) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(160) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(160) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_newframework_copy`
-- ----------------------------
DROP TABLE IF EXISTS `pre_newframework_copy`;
CREATE TABLE `pre_newframework_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(255) DEFAULT NULL COMMENT '分区名称',
  `server_name` varchar(255) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(64) DEFAULT NULL COMMENT '内网ip',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'cpu',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(160) DEFAULT NULL COMMENT '应用类型',
  `config_dir` varchar(255) DEFAULT NULL COMMENT '配置目录',
  `pro_dir` varchar(255) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(255) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(160) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(160) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_olddataback`
-- ----------------------------
DROP TABLE IF EXISTS `pre_olddataback`;
CREATE TABLE `pre_olddataback` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `fenqu` varchar(50) DEFAULT '' COMMENT '分区',
  `mingcheng` varchar(30) DEFAULT '' COMMENT '名称',
  `backmachine` varchar(50) DEFAULT '' COMMENT '备份机器',
  `backstyle` varchar(100) DEFAULT '' COMMENT '备份方式',
  `backpath` varchar(100) DEFAULT '' COMMENT '备份路径',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_online`
-- ----------------------------
DROP TABLE IF EXISTS `pre_online`;
CREATE TABLE `pre_online` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `weekday` varchar(100) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `qq` varchar(100) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `start_time` int(11) unsigned DEFAULT NULL,
  `remark` text,
  `sort` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `tel` varchar(100) DEFAULT NULL,
  `end_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_order`
-- ----------------------------
DROP TABLE IF EXISTS `pre_order`;
CREATE TABLE `pre_order` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sort` mediumint(5) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL,
  `price` mediumint(5) DEFAULT NULL,
  `buycount` mediumint(5) DEFAULT NULL,
  `paymoney` mediumint(5) DEFAULT NULL,
  `paytime` int(11) unsigned DEFAULT '0',
  `ispay` tinyint(1) DEFAULT NULL,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_pages`
-- ----------------------------
DROP TABLE IF EXISTS `pre_pages`;
CREATE TABLE `pre_pages` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `seokey` varchar(255) DEFAULT NULL,
  `sort` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `member_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `tags` varchar(255) NOT NULL,
  `content` text,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `catid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catstr` varchar(150) DEFAULT '',
  `title_in_keywords` tinyint(1) DEFAULT NULL,
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_keywords` tinyint(1) DEFAULT NULL,
  `seokey_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_title` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `menupos` varchar(20) DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(30) DEFAULT NULL,
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_pc`
-- ----------------------------
DROP TABLE IF EXISTS `pre_pc`;
CREATE TABLE `pre_pc` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `encoding` varchar(255) DEFAULT NULL COMMENT '资产编码',
  `displayencoding` varchar(255) DEFAULT NULL COMMENT '显示器编码',
  `configuration` varchar(255) DEFAULT NULL COMMENT '配置',
  `status` varchar(255) DEFAULT '开启' COMMENT '存放位置',
  `usestaus` varchar(255) DEFAULT NULL COMMENT '使用情况“1正在使用，0闲置”',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_pre_deploy_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_pre_deploy_manage`;
CREATE TABLE `pre_pre_deploy_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_product`
-- ----------------------------
DROP TABLE IF EXISTS `pre_product`;
CREATE TABLE `pre_product` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) DEFAULT NULL,
  `prospec` varchar(100) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `content` text,
  `sort` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `member_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `tags` varchar(100) DEFAULT NULL,
  `protype` varchar(100) DEFAULT NULL,
  `proprice` int(6) DEFAULT NULL,
  `stockqty` int(6) DEFAULT NULL,
  `prounit` varchar(100) DEFAULT NULL,
  `catid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `catstr` varchar(150) DEFAULT '',
  `seokey` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `buyurl` varchar(255) DEFAULT NULL,
  `title_in_keywords` tinyint(1) DEFAULT NULL,
  `title_in_description` tinyint(1) DEFAULT NULL,
  `keywords_in_title` tinyint(1) DEFAULT NULL,
  `keywords_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_keywords` tinyint(1) DEFAULT NULL,
  `seokey_in_description` tinyint(1) DEFAULT NULL,
  `seokey_in_title` tinyint(1) DEFAULT NULL,
  `urlwords_in_keywords` tinyint(1) DEFAULT NULL,
  `urlwords_in_description` tinyint(1) DEFAULT NULL,
  `urlwords_in_title` tinyint(1) DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  `menupos` varchar(20) DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author` varchar(30) DEFAULT NULL,
  `comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_propertiesofsvn`
-- ----------------------------
DROP TABLE IF EXISTS `pre_propertiesofsvn`;
CREATE TABLE `pre_propertiesofsvn` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `svn_server` varchar(100) DEFAULT '' COMMENT 'svn服务器',
  `svn_path` varchar(100) DEFAULT '' COMMENT 'svn数据路径',
  `svn_size` varchar(100) DEFAULT '' COMMENT 'svn数据大小',
  `svn_localbackuppath` varchar(100) DEFAULT '' COMMENT 'svn本地数据备份路径',
  `svn_localkeeptime` varchar(100) DEFAULT '' COMMENT 'svn本地数据备份保留时间',
  `svn_distancebackupserver` varchar(100) DEFAULT '' COMMENT 'svn异地备份服务器',
  `svn_distancebackuppath` varchar(100) DEFAULT '' COMMENT 'svn异地数据备份路径',
  `svn_distancekeeptime` varchar(100) DEFAULT '' COMMENT '异地备份保留时间',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_qunabaocom`
-- ----------------------------
DROP TABLE IF EXISTS `pre_qunabaocom`;
CREATE TABLE `pre_qunabaocom` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `gongwang_ip` varchar(64) DEFAULT NULL COMMENT '公网ip',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(64) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(64) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_qunabaoorg`
-- ----------------------------
DROP TABLE IF EXISTS `pre_qunabaoorg`;
CREATE TABLE `pre_qunabaoorg` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(64) NOT NULL DEFAULT '开启' COMMENT '内网IP',
  `mem` varchar(128) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT NULL COMMENT '应用类型',
  `gongcheng_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `config_dir` varchar(64) DEFAULT NULL COMMENT '配置目录',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT NULL COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` varchar(64) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_rm_shuju`
-- ----------------------------
DROP TABLE IF EXISTS `pre_rm_shuju`;
CREATE TABLE `pre_rm_shuju` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `gongwang` varchar(64) DEFAULT NULL COMMENT '公网ip',
  `neiwang` varchar(64) DEFAULT NULL COMMENT '内网ip',
  `yingyong` varchar(64) DEFAULT NULL COMMENT '应用',
  `denglu` varchar(64) DEFAULT NULL COMMENT '登陆地址',
  `user_password` varchar(64) DEFAULT NULL COMMENT '账号和密码',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_roboot`
-- ----------------------------
DROP TABLE IF EXISTS `pre_roboot`;
CREATE TABLE `pre_roboot` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `addr` varchar(100) DEFAULT NULL COMMENT '地区',
  `company` varchar(50) DEFAULT NULL COMMENT '保险公司',
  `type` varchar(100) DEFAULT NULL COMMENT '服务器类型',
  `pub_ip` varchar(100) DEFAULT NULL COMMENT '公网ip',
  `pri_ip` varchar(100) DEFAULT NULL COMMENT '内网ip',
  `key_1` varchar(100) DEFAULT NULL COMMENT '是否需要key',
  `key_2` varchar(100) DEFAULT NULL COMMENT '是否已插key',
  `line` varchar(100) DEFAULT NULL COMMENT '是否专线',
  `vpn` varchar(100) DEFAULT NULL COMMENT '是否需要vpn',
  `ava` varchar(100) DEFAULT NULL COMMENT '能力是否可以启用',
  `res` varchar(100) DEFAULT NULL COMMENT '未启用原因',
  `state_now` varchar(100) DEFAULT NULL COMMENT '当前机器状态',
  `user_x` varchar(100) DEFAULT NULL COMMENT '账号',
  `passwd_x` varchar(100) DEFAULT NULL COMMENT '密码',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `test` varchar(100) DEFAULT '' COMMENT '测试标题',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_scripts_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_scripts_manage`;
CREATE TABLE `pre_scripts_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `account` varchar(64) NOT NULL COMMENT '脚本创建者',
  `brirf` varchar(64) DEFAULT NULL COMMENT '简要说明',
  `goal` varchar(128) DEFAULT NULL COMMENT '脚本目的',
  `desc` varchar(128) DEFAULT NULL COMMENT '脚本详细内容',
  `is_com` varchar(15) NOT NULL COMMENT '是否在生产运行',
  `run_status` varchar(64) DEFAULT NULL COMMENT '执行情况',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_seokey`
-- ----------------------------
DROP TABLE IF EXISTS `pre_seokey`;
CREATE TABLE `pre_seokey` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `sort` mediumint(5) unsigned DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `times` mediumint(5) unsigned DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  `modid` mediumint(8) unsigned DEFAULT NULL,
  `is_title_in_url` tinyint(1) DEFAULT NULL,
  `is_title_to_pinyin` tinyint(1) DEFAULT NULL,
  `urlwords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_seowords`
-- ----------------------------
DROP TABLE IF EXISTS `pre_seowords`;
CREATE TABLE `pre_seowords` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `bs` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `wid` varchar(32) DEFAULT NULL,
  `wd` varchar(100) DEFAULT NULL,
  `dp` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_server_detail`
-- ----------------------------
DROP TABLE IF EXISTS `pre_server_detail`;
CREATE TABLE `pre_server_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称',
  `external_ip` varchar(15) DEFAULT NULL COMMENT '公网IP',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `server_id` int(11) DEFAULT NULL COMMENT '服务器ID',
  `mirr_id` int(11) DEFAULT NULL COMMENT '镜像ID',
  `pro_type` varchar(164) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(255) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(255) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(164) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(164) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(164) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `entrance_server` varchar(64) DEFAULT NULL COMMENT '所属主机',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '最后更新记录时间',
  `groupby` varchar(128) DEFAULT NULL COMMENT '分组名称',
  `local_name` varchar(200) DEFAULT NULL COMMENT '识别名称',
  `app_key` varchar(200) DEFAULT NULL COMMENT '应用唯一关键词',
  `is_deloy_dir` varchar(64) DEFAULT NULL COMMENT '发布目录判断',
  `container_dir` varchar(100) DEFAULT NULL COMMENT '容器目录',
  `java_versoin` varchar(64) DEFAULT NULL COMMENT 'java版本',
  `java_home` varchar(100) DEFAULT NULL COMMENT 'java的Home目录',
  `java_confirm` varchar(64) DEFAULT NULL COMMENT 'java版本确认',
  `auto_deloy` varchar(64) DEFAULT '0' COMMENT '是否加入自动发布',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_server_detail_20150827_1538`
-- ----------------------------
DROP TABLE IF EXISTS `pre_server_detail_20150827_1538`;
CREATE TABLE `pre_server_detail_20150827_1538` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称',
  `external_ip` varchar(15) DEFAULT NULL COMMENT '公网IP',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `server_id` int(11) DEFAULT NULL COMMENT '服务器ID',
  `mirr_id` int(11) DEFAULT NULL COMMENT '镜像ID',
  `pro_type` varchar(164) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(255) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(255) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(164) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(164) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(164) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `entrance_server` varchar(64) DEFAULT NULL COMMENT '所属主机',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '最后更新记录时间',
  `groupby` varchar(128) DEFAULT NULL COMMENT '分组名称',
  `local_name` varchar(200) DEFAULT NULL COMMENT '识别名称',
  `app_key` varchar(200) DEFAULT NULL COMMENT '应用唯一关键词',
  `is_deloy_dir` varchar(64) DEFAULT NULL COMMENT '发布目录判断',
  `container_dir` varchar(100) DEFAULT NULL COMMENT '容器目录',
  `java_versoin` varchar(64) DEFAULT NULL COMMENT 'java版本',
  `java_home` varchar(100) DEFAULT NULL COMMENT 'java的Home目录',
  `java_confirm` varchar(64) DEFAULT NULL COMMENT 'java版本确认',
  `auto_deloy` varchar(64) DEFAULT '0' COMMENT '是否加入自动发布',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_server_zw`
-- ----------------------------
DROP TABLE IF EXISTS `pre_server_zw`;
CREATE TABLE `pre_server_zw` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(64) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(64) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_svn_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_svn_manage`;
CREATE TABLE `pre_svn_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `account` varchar(64) NOT NULL COMMENT '账号',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `network_ip` varchar(15) NOT NULL COMMENT '服务器地址',
  `owner` varchar(64) DEFAULT NULL COMMENT '账号所有者',
  `owner_group` varchar(64) DEFAULT NULL COMMENT '账号所在组',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `directory_pri` varchar(255) DEFAULT NULL COMMENT '目录的权限',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_svn_uplist`
-- ----------------------------
DROP TABLE IF EXISTS `pre_svn_uplist`;
CREATE TABLE `pre_svn_uplist` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `sid` int(100) DEFAULT NULL COMMENT '升级编号',
  `bug` varchar(255) DEFAULT NULL COMMENT '提交说明（bug或者需求）',
  `num` int(100) DEFAULT NULL COMMENT '提交文件个数',
  `developer` varchar(100) DEFAULT NULL COMMENT '开发者',
  `person` varchar(255) DEFAULT NULL COMMENT '填写代码核查的相关负责人',
  `sys` varchar(255) DEFAULT NULL COMMENT '所属系统（如cm）',
  `fun` varchar(255) DEFAULT NULL COMMENT '提交所涉及的功能范围',
  `status` varchar(255) DEFAULT NULL COMMENT '程序包状态',
  `sta_1` varchar(255) DEFAULT NULL COMMENT '测试环境发布状态',
  `sta_2` varchar(255) DEFAULT NULL COMMENT '生产环境发布状态',
  `yn` varchar(255) DEFAULT NULL COMMENT '是否发生产',
  `time` varchar(100) DEFAULT NULL COMMENT '提交日期(如:2016/01/01)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_svnresourceslist`
-- ----------------------------
DROP TABLE IF EXISTS `pre_svnresourceslist`;
CREATE TABLE `pre_svnresourceslist` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `priresoulibname` varchar(255) DEFAULT '' COMMENT '一级资源库名',
  `primarysource` varchar(255) DEFAULT '' COMMENT '一级资源库路径',
  `size` varchar(100) DEFAULT '' COMMENT '一级资源库的大小',
  `pridescription` varchar(100) DEFAULT '' COMMENT '一级资源库说明',
  `secresoulibname` varchar(255) DEFAULT '' COMMENT '二级资源库名',
  `secdescription` varchar(255) DEFAULT '' COMMENT '二级资源库说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_sys_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_sys_manage`;
CREATE TABLE `pre_sys_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `account` varchar(64) NOT NULL COMMENT '账号',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `network_ip` varchar(15) NOT NULL COMMENT '服务器地址',
  `owner` varchar(64) DEFAULT NULL COMMENT '账号所有者',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `account_pri` varchar(64) DEFAULT NULL COMMENT '账号的权限',
  `account_expire` varchar(64) DEFAULT NULL COMMENT '账号有效期',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_tag`
-- ----------------------------
DROP TABLE IF EXISTS `pre_tag`;
CREATE TABLE `pre_tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `count` mediumint(6) unsigned NOT NULL,
  `module` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`) USING BTREE,
  KEY `module` (`module`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_tagged`
-- ----------------------------
DROP TABLE IF EXISTS `pre_tagged`;
CREATE TABLE `pre_tagged` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `record_id` int(11) unsigned NOT NULL,
  `tag_id` int(11) unsigned NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `module` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module` (`module`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_tbmanage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_tbmanage`;
CREATE TABLE `pre_tbmanage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `tablename` varchar(100) DEFAULT '' COMMENT '表名',
  `tabledesc` varchar(100) DEFAULT '' COMMENT '表名描述',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_template`
-- ----------------------------
DROP TABLE IF EXISTS `pre_template`;
CREATE TABLE `pre_template` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) DEFAULT NULL,
  `tpldir` varchar(50) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `layout` varchar(100) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT '',
  `sort` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `tags` varchar(100) DEFAULT NULL,
  `stylenum` smallint(6) DEFAULT NULL,
  `isdefault` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_test`
-- ----------------------------
DROP TABLE IF EXISTS `pre_test`;
CREATE TABLE `pre_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_testtab`
-- ----------------------------
DROP TABLE IF EXISTS `pre_testtab`;
CREATE TABLE `pre_testtab` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_todaylog`
-- ----------------------------
DROP TABLE IF EXISTS `pre_todaylog`;
CREATE TABLE `pre_todaylog` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `today_baidu` mediumint(8) unsigned DEFAULT '0',
  `today_google` mediumint(8) unsigned DEFAULT '0',
  `today_sogou` mediumint(8) unsigned DEFAULT '0',
  `today_soso` mediumint(8) unsigned DEFAULT '0',
  `today_yahoo` mediumint(8) unsigned DEFAULT '0',
  `today_bing` mediumint(8) unsigned DEFAULT '0',
  `module_new` mediumint(8) unsigned DEFAULT '0',
  `module_product` mediumint(8) unsigned DEFAULT '0',
  `module_down` mediumint(8) unsigned DEFAULT '0',
  `module_case` mediumint(8) unsigned DEFAULT '0',
  `module_job` mediumint(8) unsigned DEFAULT '0',
  `module_blog` mediumint(8) unsigned DEFAULT '0',
  `baidu_new` mediumint(8) unsigned DEFAULT '0',
  `baidu_product` mediumint(8) unsigned DEFAULT '0',
  `baidu_down` mediumint(8) unsigned DEFAULT '0',
  `baidu_case` mediumint(8) unsigned DEFAULT '0',
  `baidu_job` mediumint(8) unsigned DEFAULT '0',
  `baidu_blog` mediumint(8) unsigned DEFAULT '0',
  `user_new` mediumint(8) unsigned DEFAULT '0',
  `user_product` mediumint(8) unsigned DEFAULT '0',
  `user_down` mediumint(8) unsigned DEFAULT '0',
  `user_case` mediumint(8) unsigned DEFAULT '0',
  `user_job` mediumint(8) unsigned DEFAULT '0',
  `user_blog` mediumint(8) unsigned DEFAULT '0',
  `site_baidu_count` mediumint(8) unsigned DEFAULT '0',
  `site_google_count` mediumint(8) unsigned DEFAULT '0',
  `site_sogou_count` mediumint(8) unsigned DEFAULT '0',
  `site_soso_count` mediumint(8) unsigned DEFAULT '0',
  `site_yahoo_count` mediumint(8) unsigned DEFAULT '0',
  `site_bing_count` mediumint(8) unsigned DEFAULT '0',
  `site_baidu_index` mediumint(8) unsigned DEFAULT '0',
  `site_google_index` mediumint(8) unsigned DEFAULT '0',
  `site_sogou_index` mediumint(8) unsigned DEFAULT '0',
  `site_soso_index` mediumint(8) unsigned DEFAULT '0',
  `site_yahoo_index` mediumint(8) unsigned DEFAULT '0',
  `site_bing_index` mediumint(8) unsigned DEFAULT '0',
  `site_baidu_plus` mediumint(8) unsigned DEFAULT '0',
  `site_google_plus` mediumint(8) unsigned DEFAULT '0',
  `site_sogou_plus` mediumint(8) unsigned DEFAULT '0',
  `site_soso_plus` mediumint(8) unsigned DEFAULT '0',
  `site_yahoo_plus` mediumint(8) unsigned DEFAULT '0',
  `site_bing_plus` mediumint(8) unsigned DEFAULT '0',
  `total_user` mediumint(8) unsigned DEFAULT '0',
  `total_spider` mediumint(8) unsigned DEFAULT '0',
  `total_count` mediumint(8) unsigned DEFAULT '0',
  `baidu_br` mediumint(8) unsigned DEFAULT '0',
  `google_pr` mediumint(8) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_tplstyle`
-- ----------------------------
DROP TABLE IF EXISTS `pre_tplstyle`;
CREATE TABLE `pre_tplstyle` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) DEFAULT NULL,
  `styledir` varchar(100) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `create_time` int(11) unsigned DEFAULT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT '',
  `sort` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `tags` varchar(100) DEFAULT NULL,
  `industry` varchar(100) DEFAULT NULL,
  `colors` varchar(100) DEFAULT NULL,
  `tplid` mediumint(8) unsigned DEFAULT NULL,
  `tpldir` varchar(50) DEFAULT NULL,
  `isdefault` tinyint(1) unsigned DEFAULT NULL,
  `numberID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_unusedcomputer`
-- ----------------------------
DROP TABLE IF EXISTS `pre_unusedcomputer`;
CREATE TABLE `pre_unusedcomputer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(100) DEFAULT '' COMMENT '分区名称',
  `server_name` varchar(100) DEFAULT '' COMMENT '服务器名称或域名',
  `network_ip` varchar(100) DEFAULT '' COMMENT '内网ip',
  `cpu` varchar(100) DEFAULT '' COMMENT 'cpu',
  `mem` varchar(64) DEFAULT '' COMMENT '内存',
  `disk` varchar(64) DEFAULT '' COMMENT '数据盘',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `pro_port` varchar(64) DEFAULT '' COMMENT '启动端口',
  `pro_init` varchar(100) DEFAULT '' COMMENT '启动脚本',
  `pro_key` varchar(100) DEFAULT '' COMMENT '进程关键词',
  `log_dir` varchar(100) DEFAULT '' COMMENT '日志路径',
  `config_dir` varchar(100) DEFAULT '' COMMENT '配置目录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_urls`
-- ----------------------------
DROP TABLE IF EXISTS `pre_urls`;
CREATE TABLE `pre_urls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(200) DEFAULT NULL,
  `modid` mediumint(8) unsigned DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`url`) USING BTREE,
  KEY `module` (`module`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_user`
-- ----------------------------
DROP TABLE IF EXISTS `pre_user`;
CREATE TABLE `pre_user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(64) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `last_login_time` int(11) unsigned DEFAULT '0',
  `last_login_ip` varchar(40) DEFAULT NULL,
  `login_count` mediumint(8) unsigned DEFAULT '0',
  `email` varchar(50) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `pre_user`
-- ----------------------------
BEGIN;
INSERT INTO `pre_user` VALUES ('1', 'admin', '管理员', 'e10adc3949ba59abbe56e057f20f883e', '1533222831', '10.68.4.50', '1362', '530035210@qq.com', '', '1412953210', '1412953210', '1');
COMMIT;

-- ----------------------------
--  Table structure for `pre_viewlog`
-- ----------------------------
DROP TABLE IF EXISTS `pre_viewlog`;
CREATE TABLE `pre_viewlog` (
  `id` mediumint(5) unsigned NOT NULL AUTO_INCREMENT,
  `request_method` varchar(30) DEFAULT NULL,
  `http_user_agent` varchar(255) DEFAULT NULL,
  `spider_code` varchar(30) DEFAULT NULL,
  `request_time` int(11) unsigned NOT NULL DEFAULT '0',
  `module` varchar(30) DEFAULT NULL,
  `modid` mediumint(8) unsigned DEFAULT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `module_action` varchar(30) DEFAULT NULL,
  `request_uri` varchar(100) DEFAULT NULL,
  `server_protocol` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_vpn_manage`
-- ----------------------------
DROP TABLE IF EXISTS `pre_vpn_manage`;
CREATE TABLE `pre_vpn_manage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `account` varchar(64) NOT NULL COMMENT '账号',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `network_ip` varchar(15) NOT NULL COMMENT 'VPN服务器地址',
  `owner` varchar(64) DEFAULT NULL COMMENT '账号所有者',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `expire_date` varchar(64) DEFAULT NULL COMMENT '账号的有效期',
  `status` varchar(64) DEFAULT '启用' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_wuxianjiankong`
-- ----------------------------
DROP TABLE IF EXISTS `pre_wuxianjiankong`;
CREATE TABLE `pre_wuxianjiankong` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `ap` varchar(255) DEFAULT NULL COMMENT '设备类型',
  `admin` varchar(255) DEFAULT NULL COMMENT '管理账号',
  `password` varchar(255) DEFAULT NULL COMMENT '管理密码',
  `ssid` varchar(255) DEFAULT NULL COMMENT 'SSID',
  `passwd` varchar(255) DEFAULT NULL COMMENT 'SSID密码',
  `spectrum` varchar(255) DEFAULT NULL COMMENT '无线频段',
  `address` varchar(255) DEFAULT NULL COMMENT '管理ip',
  `floor` varchar(255) DEFAULT NULL COMMENT '设备放置的楼层',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_xenserverlist`
-- ----------------------------
DROP TABLE IF EXISTS `pre_xenserverlist`;
CREATE TABLE `pre_xenserverlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `type/version` varchar(255) DEFAULT NULL COMMENT '类型/版本',
  `ipaddr` varchar(255) DEFAULT NULL COMMENT '管理机IP地址',
  `account/passwd` varchar(255) DEFAULT NULL COMMENT '账号/密码',
  `t-mem/re-mem` varchar(255) DEFAULT NULL COMMENT '总内存/剩余内存',
  `t-disk/re-disk` varchar(255) DEFAULT NULL COMMENT '总硬盘/剩余硬盘',
  `vm` varchar(255) DEFAULT NULL COMMENT '虚拟机个数',
  `createvm` varchar(255) DEFAULT NULL COMMENT '可以创建虚拟机(50G-2G)个数',
  `status` varchar(64) DEFAULT '开启' COMMENT '虚拟机总数/可创建虚拟机总数',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_xinkuangjia_com`
-- ----------------------------
DROP TABLE IF EXISTS `pre_xinkuangjia_com`;
CREATE TABLE `pre_xinkuangjia_com` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(128) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `gongwang_ip` varchar(64) DEFAULT NULL COMMENT '公网ip',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(64) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(64) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_xinkuangjia_com_copy`
-- ----------------------------
DROP TABLE IF EXISTS `pre_xinkuangjia_com_copy`;
CREATE TABLE `pre_xinkuangjia_com_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `depart_name` varchar(128) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `gongwang_ip` varchar(64) DEFAULT NULL COMMENT '公网ip',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(64) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(64) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(64) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(64) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(64) DEFAULT NULL COMMENT '启动端口',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_yanfarecord`
-- ----------------------------
DROP TABLE IF EXISTS `pre_yanfarecord`;
CREATE TABLE `pre_yanfarecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `dep` varchar(100) DEFAULT '' COMMENT '部门',
  `name` varchar(100) DEFAULT '' COMMENT '姓名',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `sys` varchar(100) DEFAULT '' COMMENT '所属系统',
  `func` varchar(100) DEFAULT '' COMMENT '负责功能',
  `tel` varchar(100) DEFAULT '' COMMENT '联系方式',
  `note` varchar(128) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pre_yanfarecord-bak`
-- ----------------------------
DROP TABLE IF EXISTS `pre_yanfarecord-bak`;
CREATE TABLE `pre_yanfarecord-bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '开启' COMMENT '使用状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `region`
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `district_name` varchar(255) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`) USING BTREE,
  CONSTRAINT `region_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `rex_52zzb_hosts`
-- ----------------------------
DROP TABLE IF EXISTS `rex_52zzb_hosts`;
CREATE TABLE `rex_52zzb_hosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `local_name` varchar(200) DEFAULT NULL COMMENT '唯一标识符',
  `app_key` varchar(200) DEFAULT NULL,
  `depart_name` varchar(64) NOT NULL COMMENT '分区名称',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称或域名',
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `cpu` varchar(64) DEFAULT NULL COMMENT 'CPU',
  `mem` varchar(64) DEFAULT NULL COMMENT '内存',
  `disk` varchar(64) DEFAULT NULL COMMENT '数据盘',
  `pro_type` varchar(64) DEFAULT '' COMMENT '应用类型',
  `config_dir` varchar(164) DEFAULT '' COMMENT '配置目录',
  `pro_dir` varchar(164) DEFAULT NULL COMMENT '工程目录',
  `log_dir` varchar(164) DEFAULT NULL COMMENT '日志路径',
  `pro_key` varchar(64) DEFAULT NULL COMMENT '进程关键词',
  `pro_init` varchar(100) DEFAULT NULL COMMENT '启动脚本',
  `pro_port` varchar(255) DEFAULT NULL COMMENT '启动端口',
  `is_deloy_dir` varchar(64) DEFAULT NULL COMMENT '2:代表发布工程和配置目录,且工程和配置处于同级目录  1:代表只发布工程，且配置目录包含在工程目录之中。',
  `system_type` varchar(64) DEFAULT NULL COMMENT '操作系统',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_time` datetime DEFAULT NULL COMMENT '更新记录的时间',
  `status` varchar(64) DEFAULT '启用' COMMENT '状态',
  `note` varchar(128) DEFAULT NULL COMMENT '备注',
  `mask` int(12) DEFAULT NULL COMMENT '唯一标志位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tmp`
-- ----------------------------
DROP TABLE IF EXISTS `tmp`;
CREATE TABLE `tmp` (
  `network_ip` varchar(15) NOT NULL COMMENT '内网IP',
  `server_name` varchar(128) DEFAULT NULL COMMENT '服务器名称',
  `server_id` int(11) DEFAULT NULL COMMENT '服务器ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
